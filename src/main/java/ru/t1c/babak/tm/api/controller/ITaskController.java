package ru.t1c.babak.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void createTask();

    void clearTasks();

}

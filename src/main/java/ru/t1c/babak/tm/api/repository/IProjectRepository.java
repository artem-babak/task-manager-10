package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project Project);

    Project create(String name);

    Project create(String name, String description);

    void remove(Project Project);

    void clear();

}

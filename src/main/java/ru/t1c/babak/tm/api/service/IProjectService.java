package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void remove(Project project);

    void clear();

}

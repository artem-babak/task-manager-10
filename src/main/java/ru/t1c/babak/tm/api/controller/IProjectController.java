package ru.t1c.babak.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void createProject();

    void clearProjects();

}

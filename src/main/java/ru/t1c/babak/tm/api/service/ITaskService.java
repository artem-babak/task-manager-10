package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void remove(Task task);

    void clear();

}

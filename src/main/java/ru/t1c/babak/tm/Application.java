package ru.t1c.babak.tm;

import ru.t1c.babak.tm.component.Bootstrap;

/**
 * Simple task manager application.
 *
 * @author Artem Babak
 */
public final class Application {

    public static void main(final String[] args) {
        new Bootstrap().run(args);
    }

}

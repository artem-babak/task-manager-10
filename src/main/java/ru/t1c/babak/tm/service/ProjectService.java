package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty() ||
                description == null || description.isEmpty())
            return null;
        return projectRepository.create(name, description);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
